package com.techbrothers.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DisplayController {

	
	@GetMapping("/data")
	public String getData() {
	
		return "data";
	}

}
